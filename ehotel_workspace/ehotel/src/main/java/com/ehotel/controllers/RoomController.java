package com.ehotel.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.ehotel.entity.user.Room;
import com.ehotel.service.room.RoomService;
import com.ehotel.validation.FormValidationGroup;

@Controller
public class RoomController {

	@Autowired
	private RoomService roomService;

	@RequestMapping(value = "searchRoom/submit", method = { RequestMethod.POST })
	public ModelAndView searchRoom(@RequestParam("id") int id, Model model) {
		ModelAndView mv = new ModelAndView();
		System.out.println("from room controller");
		int crit = id;

		Room roomResult = roomService.searchRoom(crit);
		if (roomResult == null) {
			model.addAttribute("NoResult", "No rooms found with this number");
		} else {
			model.addAttribute("roomResult", roomResult);
		}

		mv.setViewName("employee");
		mv.addObject("Room", new Room());
		return mv;
	}

	
	@RequestMapping(value = "addRoom/submit", method = { RequestMethod.POST })
	public ModelAndView addRoom(@ModelAttribute("room") @Validated(FormValidationGroup.class) Room room,
			BindingResult bindingResult, Model model) {
		ModelAndView mv = new ModelAndView();
		System.out.println("error from add room controller");
		
		if (bindingResult.hasErrors()) {

			System.out.println("error from add room controller");
			mv.addObject("NoResult", "The room cannot be added, please try again");
			mv.setViewName("employee");
		} else if (roomService.addRoom(room)){
			
			mv.addObject("Result", "The room has been added successfully");
			mv.setViewName("employee");
		} else {
			mv.addObject("NoResult", "This room number already exist");
			mv.setViewName("employee");
		}
		return mv;
	}
}
