package com.ehotel.controllers;

import java.security.Principal;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ehotel.entity.user.Room;
import com.ehotel.entity.user.User;
import com.ehotel.service.reservation.ReservationService;
import com.ehotel.service.user.UsersService;
import com.ehotel.validation.FormValidationGroup;

@Controller
public class LoginController {

	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	private ReservationService reservationService;

	@Autowired
	public void setReservationService(ReservationService reservationService) {
		this.reservationService = reservationService;
	}
	
	///////
	@RequestMapping(value={"/customer**"})
	public ModelAndView viewWelcomePage(Principal user, HttpServletRequest req){
		
		ModelAndView mv = new ModelAndView();
		String username = user.getName();
		
		System.out.println("from customer controller");

		List<Map<String,Object>> userReservations = reservationService.getAllReservations(username);
		System.out.println("from controller"+userReservations.toString());
		mv.addObject("userReservations", userReservations);
		mv.setViewName("customer");
		
		HttpSession session = req.getSession();
		session.setAttribute("username", username);
		session.setAttribute("userReservations", userReservations);
		mv.addObject("username", username);
		
		mv.addObject("tabClasshome", "active");
		mv.addObject("tabClassf2", "");
		mv.addObject("tabClassf1", "");
		
		mv.addObject("fadeInActivehome", "tab-pane fade in active");
		mv.addObject("fadeInActivef1", "tab-pane fade");
		mv.addObject("fadeInActivef2", "tab-pane fade");
		return mv; 
	}
	
	@RequestMapping(value="/employee**")
	public ModelAndView viewAdminPage(Principal user, HttpServletRequest req){
		
		String username = user.getName();
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("employee");
		mv.addObject("username", username);
		mv.addObject("Room", new Room());
		HttpSession session = req.getSession();
		session.setAttribute("username", username);
		return mv;
	}
	////////////
	
	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "loggedout";
	}
	
	@RequestMapping("/denied")
	public String showDenied() {
		return "denied";
	}
	
	@RequestMapping("/newaccount")
	public String showNewAccount(Model model) {

		model.addAttribute("user", new User());
		return "newaccount";
	}

	@RequestMapping(value = "/createaccount", method = RequestMethod.POST)
	public String createAccount(@Validated(FormValidationGroup.class) User user, BindingResult result) {

		if (result.hasErrors()) {
			return "newaccount";
		}

		user.setAuthority("ROLE_USER");
		user.setEnabled(true);

		if (usersService.exists(user.getUsername())) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		try {
			usersService.create(user);
		} catch (DuplicateKeyException e) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		return "accountcreated";
	}
}
