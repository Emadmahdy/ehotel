package com.ehotel.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

public class AuthSuccessHandler 
	extends SavedRequestAwareAuthenticationSuccessHandler{

	@Override
	protected String determineTargetUrl(HttpServletRequest request, HttpServletResponse response) {

		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		String targetUrl = "";
		
		if (auth == null){    

		       new SecurityContextLogoutHandler().logout(request, response, auth);
		       return targetUrl = "/logout";
		    } else {
					String role = auth.getAuthorities().toString();
					
					System.out.println(role);
						if(role.equalsIgnoreCase("[ROLE_USER]")){
							targetUrl = "/customer";
						}else if(role.equalsIgnoreCase("[ROLE_ADMIN]")){
							targetUrl = "/employee";
						}				
						return targetUrl;
		}
		
	}
}




