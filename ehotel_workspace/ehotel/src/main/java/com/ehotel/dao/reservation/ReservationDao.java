package com.ehotel.dao.reservation;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ehotel.entity.user.Reservation;
import com.ehotel.entity.user.Room;
import com.ehotel.entity.user.User;

@Repository
@Component("reservationDao")
public class ReservationDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session() {
		return sessionFactory.getCurrentSession();
	}

	@Transactional
	public List<Map<String, Object>> getAllReservations(String username) {
		/*
		 * //Criteria criteria = session().createCriteria(Reservation.class);
		 * //criteria.createAlias("reservation", "r");
		 * criteria.createAlias("user", "u");
		 * criteria.add(Restrictions.eq("username.username", username));
		 * criteria.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP);
		 */

		String query = "SELECT {u.*}, {re.*}, {ro.*}  from reservation re, room ro, users u"
				+ " where re.USERNAME = :username " + "AND u.USERNAME = re.USERNAME " + "AND ro.ID = re.ROOMID";

		@SuppressWarnings("unchecked")
		List<Map<String, Object>> userReservations = session().createSQLQuery(query).addEntity("re", Reservation.class)
				.addEntity("ro", Room.class).addEntity("u", User.class).setString("username", username)
				.setResultTransformer(Criteria.ALIAS_TO_ENTITY_MAP).list();

		System.out.println("from DAO" + userReservations);
		return userReservations;
	}

	@SuppressWarnings("unchecked")
	@Transactional
	public List<Room> searchAvailRoom(Date fromDate, Date toDate, int beds) {
		
		String query = "  SELECT  ro.* from  reservation re, room ro "
				+ "where ro.beds <= :beds "
				+ "AND ro.ID = re.ROOMID (+) "
				+ "AND ro.id not in ( "
					+ "SELECT roomid FROM RESERVATION re "
					+ "WHERE "
						+ "re.FROMDATE <= :toDate "
						+ "AND re.TODATE >= :fromDate "
				+ ")";
		List<Room> availRooms = session()
				.createSQLQuery(query)
				.addEntity(Room.class)
				.setInteger("beds", beds)
				.setDate("fromDate", fromDate)
				.setDate("toDate", toDate)
				.list();
		
		System.out.println(availRooms);
		return availRooms;
	}

	@Transactional
	public void addRes(Reservation res) {
		session().save(res);
		
	}

}
