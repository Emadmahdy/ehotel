package com.ehotel.dao.room;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.ehotel.entity.user.Room;

@Repository
@Component("roomDao")
public class RoomDao {

	@Autowired
	private SessionFactory sessionFactory;

	public Session session(){
		return sessionFactory.getCurrentSession();
	}
	
	@Transactional
	public Room searchRoom(int id) {
		
		Criteria criteria = session().createCriteria(Room.class);
		criteria.add(Restrictions.eq("id", id));
		Room roomResult = (Room) criteria.uniqueResult(); 
		
		return roomResult;
	}

	@Transactional
	public boolean addRoom(Room room) {
		Criteria crit = session().createCriteria(Room.class);
		crit.add(Restrictions.eq("id", room.getId()));
		if (crit.uniqueResult() != null) {
			return false;
		} else {
			return session().save(room) != null;
		}
		
	}	
}
