package com.ehotel.service.room;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.ehotel.dao.room.RoomDao;
import com.ehotel.entity.user.Room;

@Service
@Component(value="roomService")
public class RoomService {
	
	@Autowired
	private RoomDao roomDao;

	public Room searchRoom(int crit) {
		Room roomResult = roomDao.searchRoom(crit);
		return roomResult;
	}

	public boolean addRoom(Room room) {
		
		if (roomDao.addRoom(room)) {
			return true;
		} else return false;
		
	}
}
