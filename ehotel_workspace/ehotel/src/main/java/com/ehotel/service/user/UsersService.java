package com.ehotel.service.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ehotel.dao.user.UsersDao;
import com.ehotel.entity.user.User;

@Service("usersService")
public class UsersService {

	private UsersDao usersDao;

	@Autowired
	public void setOffersDao(UsersDao usersDao) {
		this.usersDao = usersDao;
	}

	public boolean exists(String username) {
		return usersDao.exists(username);
	}

	public void create(User user) {
		usersDao.create(user);

	}

	
}
