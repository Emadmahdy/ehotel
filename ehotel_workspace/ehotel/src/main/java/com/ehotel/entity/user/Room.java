package com.ehotel.entity.user;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import com.ehotel.validation.FormValidationGroup;
import com.ehotel.validation.PersistenceValidationGroup;

@Entity
@Table(name="room")
public class Room {

	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@Id
	@Column(name="id")
	private int id;

	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@Pattern(regexp="^[0-9]*$")
	@Column(name="beds")
	private int beds;

	@Override
	public String toString() {
		return "Room [id=" + id + ", beds=" + beds + "]";
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getBeds() {
		return beds;
	}

	public void setBeds(int beds) {
		this.beds = beds;
	}
	
	
}
