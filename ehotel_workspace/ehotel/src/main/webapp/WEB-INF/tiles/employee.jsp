<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>


<div class="container">
	<h3>Employee home page</h3>
	<h5 class="welcome">Welcome ${username}</h5>

	<ul class="nav nav-tabs">
		<li class="active"><a data-toggle="tab" href="#home">Home</a></li>
		<li><a data-toggle="tab" href="#f1">Add Room</a></li>
		<li><a data-toggle="tab" href="#f2">Search Rooms</a></li>
	</ul>

	<div class="tab-content">

		<div id="home" class="tab-pane fade in active">
			<c:if test="${not empty NoResult }">
				<h2 class="error">${NoResult}</h2>
			</c:if>
			<c:if test="${not empty Result }">
				<h2 class="success">${Result}</h2>
			</c:if>
			<c:if test="${ not empty roomResult }">
				<table class="table ">
					<tr>
						<th>Room Number</th>
						<th>Number of beds</th>
					</tr>
					<tr>
						<td>${roomResult.id}</td>
						<td>${roomResult.beds}</td>
					</tr>
				</table>

			</c:if>

		</div>

		<div id="f1" class="tab-pane fade">
			<form:form
				action="${pageContext.request.contextPath }/addRoom/submit"
				method="post" class="form-horizontal" modelAttribute="room">

				<div class="form-group">
					<label for="id" class="col-sm-2 control-label">Room No. :</label> <input
						type="number" name="id" id="id" class="col-sm-4" />
				</div>
				<div class="form-group">
					<label for="id" class="col-sm-2 control-label">Number of beds:</label> <input
						type="number" name="beds" id="beds" class="col-sm-4" />
				</div>
				<div class="form-group">
					<button type="submit" value="Add room"
						class="btn btn-primary btn-lg">Add room</button>
				</div>
			</form:form>
		</div>

		<div id="f2" class="tab-pane fade">
			<form:form
				action="${pageContext.request.contextPath }/searchRoom/submit"
				method="post" class="form-horizontal">

				<div class="form-group">
					<label for="id" class="col-sm-2 control-label">Room No. :</label> <input
						type="number" name="id" id="id" class="col-sm-4" />
				</div>
				<div class="form-group">
					<button type="submit" value="Search for room"
						class="btn btn-primary btn-lg">Search for room</button>
				</div>
			</form:form>
		</div>
	</div>
</div>
