package com.ehotel.entity.user;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.ehotel.validation.FormValidationGroup;
import com.ehotel.validation.PersistenceValidationGroup;

@Entity
@Table(name="reservation")
public class Reservation {

	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQUENCERes")
	@SequenceGenerator(name="SEQUENCERes", sequenceName="RESERVATION_SEQ", allocationSize=1)
	@Column(name="rid")
	private int rid;

	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@ManyToOne
	@JoinColumn(name="id")
	private Room roomId;
	
	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@ManyToOne
	@JoinColumn(name="username")
	private String username;

	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@Column(name="fromDate")
	private Date fromDate;
	
	@NotNull(groups={PersistenceValidationGroup.class, FormValidationGroup.class})
	@Column(name="toDate")
	private Date toDate;

	@Override
	public String toString() {
		return "Reservation [rid=" + rid + ", roomId=" + roomId + ", username=" + username + ", fromDate=" + fromDate
				+ ", toDate=" + toDate + "]";
	}

	public int getRid() {
		return rid;
	}

	public void setRid(int rid) {
		this.rid = rid;
	}

	public Room getRoomId() {
		return roomId;
	}

	public void setRoomId(Room roomId) {
		this.roomId = roomId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}
	
	
	
	
}
