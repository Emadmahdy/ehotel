package com.ehotel.controllers;

import java.security.Principal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.ehotel.entity.user.Room;
import com.ehotel.entity.user.User;
import com.ehotel.service.user.UsersService;
import com.ehotel.validation.FormValidationGroup;

@Controller
public class LoginController {

	private UsersService usersService;

	@Autowired
	public void setUsersService(UsersService usersService) {
		this.usersService = usersService;
	}

	
	///////
	@RequestMapping(value={"/customer**"})
	public ModelAndView viewWelcomePage(Principal user){
		
		ModelAndView mv = new ModelAndView();
		
		String username = user.getName();
		
		mv.setViewName("customer");
		mv.addObject("username", username);		
		return mv;
	}
	
	@RequestMapping(value="/employee**")
	public ModelAndView viewAdminPage(Principal user, HttpServletRequest req){
		
		String username = user.getName();
		
		ModelAndView mv = new ModelAndView();
		mv.setViewName("employee");
		mv.addObject("username", username);
		mv.addObject("Room", new Room());
		HttpSession session = req.getSession();
		session.setAttribute("username", username);
		return mv;
	}
	////////////
	
	@RequestMapping("/login")
	public String showLogin() {
		return "login";
	}

	@RequestMapping("/loggedout")
	public String showLoggedOut() {
		return "loggedout";
	}
	
	@RequestMapping("/denied")
	public String showDenied() {
		return "denied";
	}
	
	@RequestMapping("/newaccount")
	public String showNewAccount(Model model) {

		model.addAttribute("user", new User());
		return "newaccount";
	}

	@RequestMapping(value = "/createaccount", method = RequestMethod.POST)
	public String createAccount(@Validated(FormValidationGroup.class) User user, BindingResult result) {

		if (result.hasErrors()) {
			return "newaccount";
		}

		user.setAuthority("ROLE_USER");
		user.setEnabled(true);

		if (usersService.exists(user.getUsername())) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		try {
			usersService.create(user);
		} catch (DuplicateKeyException e) {
			result.rejectValue("username", "DuplicateKey.user.username");
			return "newaccount";
		}

		return "accountcreated";
	}
}
