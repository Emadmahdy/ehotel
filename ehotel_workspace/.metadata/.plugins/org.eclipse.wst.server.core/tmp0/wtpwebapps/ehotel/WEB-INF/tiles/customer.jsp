<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="com.ehotel.entity.user.Reservation"%>



<div class="container">
	<h3>Customer home page</h3>
	<h5 class="welcome">Welcome ${username}</h5>

	<ul class="nav nav-tabs">
		<li class="${tabClasshome}"><a data-toggle="tab" href="#home">My
				reservations</a></li>
		<%-- <li class="${tabClassf1}"><a data-toggle="tab" href="#f1">Make a reservation</a></li> --%>
		<li class="${tabClassf2}"><a data-toggle="tab" href="#f2">Search Rooms</a></li>
	</ul>

	<div class="tab-content">

		<div id="home" class="${fadeInActivehome }">
			
			<c:if test="${ empty userReservations }">
				<p class="error">You do not have any reservations yet</p>
			</c:if>
			<c:if test="${ not empty userReservations }">
				<table class="table ">
					<tr>
						<th>Reservation No.</th>
						<th>Guest</th>
						<th>Room Number</th>
						<th>From</th>
						<th>To</th>
					</tr>
					<%
						@SuppressWarnings("unchecked")
							List<Map<String, Object>> userReservations = (List<Map<String, Object>>) session
									.getAttribute("userReservations");
							for (Map<String, Object> row : userReservations) {

								Reservation res = (Reservation) row.get("re");

								SimpleDateFormat ft = new SimpleDateFormat("E MMM dd,yy ' @ ' hh:mm a zzz");

								out.print("<tr><td>" + res.getRid() + "</td>" + "<td>" + res.getUsername().getUsername() + "</td>"
										+ "<td>" + res.getRoomId().getId() + "</td>" + "<td>" + ft.format(res.getFromDate())
										+ "</td>" + "<td>" + ft.format(res.getToDate()) + "</td></tr>");
							}
					%>
				</table>
			</c:if>
		</div>

		<%-- <div id="f1" class="${fadeInActivef1}">
			
		</div> --%>

		<div id="f2" class="${fadeInActivef2}">
		
		<c:if test="${not empty successReservation }">
			<p class="success">${successReservation }</p>
		</c:if>
		
			<form
				action="${pageContext.request.contextPath }/searchAvailRoom/submit"
				method="post" class="form-horizontal"
				id="searchAvailRoomForm">

					<label for="fromDate" class="col-sm-2 control-label">From: </label>
					<div class='col-md-4'>
						<div class="form-group">
							<div class='input-group date' id='datetimepicker6'>
								<input type='text' class="form-control" name="fromDate"/> <span
									class="input-group-addon"> <span
									class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
					
					<label for="toDate" class="col-sm-2 control-label">To: </label>
					<div class='col-md-4'>
						<div class="form-group">
							<div class='input-group date' id='datetimepicker7'>
								<input type='text' class="form-control" name="toDate"/> 
								<span class="input-group-addon"> 
								<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div>
					</div>
				

				<div class="form-group">
					<label for="id" class="col-sm-2 control-label">Number of guests :</label> 
						<div class='col-md-4'><input type="number" class="form-control" name="beds" id="beds"
						class="col-sm-4" /></div>
				</div>

				<div class="form-group">
					<button type="submit" value="Search for room"
						class="btn btn-primary btn-lg">Search rooms</button>
				</div>
			</form>
			
			<c:if test="${not empty availRooms }">
				<table class="table ">
					<tr><th>Room No.</th><th>Number of beds</th><th>Action</th></tr>
					<c:forEach var="room" items="${availRooms}">
					<tr>
						<form action="${pageContext.request.contextPath }/selectRoom/submit"
									method="post" class="form-horizontal">
							<td><input class="inputTableForm" name="roomid" readonly value="${room.id}"/></td>
							<td><input class="inputTableForm" name="beds" disabled="disabled" value="${room.beds}"/></td>
							<td><button type="submit" class="btn btn-info">Select</button></td>
						</form>
					</tr>
					</c:forEach>
				</table>
			</c:if>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(function() {
		$('#datetimepicker6').datetimepicker();
		$('#datetimepicker7').datetimepicker({
			useCurrent : false
		//Important! See issue #1075
		});
		$("#datetimepicker6").on("dp.change", function(e) {
			$('#datetimepicker7').data("DateTimePicker").minDate(e.date);
		});
		$("#datetimepicker7").on("dp.change", function(e) {
			$('#datetimepicker6').data("DateTimePicker").maxDate(e.date);
		});
	});
	
	 var frmvalidator = new Validator("searchAvailRoomForm");
	 frmvalidator.addValidation("fromDate","req","Please select a starting date");
	 frmvalidator.addValidation("beds","num","Please select a starting date");
	 frmvalidator.addValidation("toDate","req","Please select an ending date");
	 frmvalidator.addValidation("beds","req","Please enter the number of guests");

</script>

